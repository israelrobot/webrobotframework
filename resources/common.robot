*** Settings ***
Library     SeleniumLibrary

*** Variable ***
${BROWSER}      chrome

###Opções necessárias para rodar headless no linux de CI (runner)
${OPTIONS}    add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Keywords ***
### Setup e Teardown ###
Abrir navegador
    Open Browser    about:blank      ${BROWSER}   options=${OPTIONS}
    Maximize Browser Window

Fechar navegador
    Close Browser
